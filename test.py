import psutil

def WindowExists(classname):
	try:
		classname in ( p.name() for p in psutil.process_iter() )
	except psutil.NoSuchProcess:
		return False
	else:
		return True

for pid in psutil.pids():
    p = psutil.Process(pid)
    print p