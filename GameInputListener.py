import sys, keyboard, pygame

class GameInputListener:

	def __init__(self, time_limit=-1):
		self.keys = []
		self.key_states = dict()
		self.key_actions = dict()

		self.buttons = []
		self.button_states = dict()
		self.button_actions = dict()

		self.quit_flag = False
		self.timer_active = False
		self.timer_began_callback = lambda:None
		self.timer_ended_callback = lambda:None
		if (not (type(time_limit) is int)) or time_limit <= 0:
			self.time_limit = -1
		else:
			self.time_limit = time_limit
		self.start_time = 0

	def __del__(self):
		self.Dispose()

	def Dispose(self):
		self.timer_began_callback = None
		self.timer_ended_callback = None
		self.keys = []
		self.buttons = []
		self.key_states.clear()
		self.button_states.clear()
		self.key_actions.clear()
		self.button_actions.clear()
		try:
			keyboard.unhook_all()
		except:
			raise

	def Listen(self):
		key_pressed = False
		current_ticks = pygame.time.get_ticks()
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				gil.Dispose()
				return False
			elif event.type == pygame.KEYDOWN:
				key_pressed = True
				if event.key in self.keys:
					if self.key_states[event.key]:
						self.key_actions[event.key]()
			elif event.type == pygame.JOYBUTTONDOWN:
				key_pressed = True
				if event.button in self.buttons:
					if self.button_states[event.button]:
						self.button_actions[event.button]()
		if self.quit_flag:
			gil.Dispose()
			return False
		else:
			if self.timer_active:
				if key_pressed:
					self.start_time = current_ticks
				if self.time_limit > 0:
					if current_ticks - self.start_time >= self.time_limit:
						self.StopTimer()
			else:
				if key_pressed:
					self.StartTimer(current_ticks)
		return True

	def AddTimerBeganCallback(self, callback):
		if callable(callback):
			self.timer_began_callback = callback;

	def AddTimerEndedCallback(self, callback):
		if callable(callback):
			self.timer_ended_callback = callback;

	def StartTimer(self, ticks):
		if self.time_limit > 0:
			self.start_time = ticks
			self.timer_active = True
			self.timer_began_callback()

	def StopTimer(self):
		if self.time_limit > 0:
			self.timer_active = False
			self.timer_ended_callback()

	def Quit(self):
		self.quit_flag = True

	def AddQuitKey(self, key):
		self.AddKeyBinding(key, self.Quit)

	def AddQuitButton(self, button):
		self.AddButtonBinding(button, self.Quit)

	def Key_Callback(self, key):
		if key in self.keys:
			if self.key_states[key]:
				key_event = pygame.event.Event(pygame.KEYDOWN, {'key':key})
				pygame.event.post(key_event)

	def AddKeyBinding(self, key, callback):
		if not callable(callback):
			self.Dispose()
			raise TypeError("callback argument in AddKeyBinding is not a callable function")
		try:
			action_callback = lambda:self.Key_Callback(key)
			keyboard.hook_key(key, action_callback)
			if not (key in self.keys):
				self.keys.append(key)
				self.key_states[key] = True
			self.key_actions[key] = callback
		except:
			self.Dispose()
			raise

	def AddButtonBinding(self, button, callback):
		if not callable(callback):
			self.Dispose()
			raise TypeError("callback argument in AddButtonBinding is not a callable function")
		if not (button in self.buttons): 
			self.buttons.append(button)
			self.button_states[button] = True
		self.button_actions[button] = callback


def PrintKDown():
	print "k - down"

def PrintAButtonDown():
	print "a button - down"

def PrintTimerBegin():
	print "timer started"

def PrintTimerEnded():
	print "timer ended"

if __name__ == "__main__":
	pygame.init()
	pygame.event.set_grab(True)
	# pygame.display.set_mode((100, 100))
	
	pygame.joystick.init()
	controller = pygame.joystick.Joystick(0)
	controller.init()
	clock = pygame.time.Clock()

	#setup the listener
	gil = GameInputListener(2000)
	gil.StartTimer(pygame.time.get_ticks())
	gil.AddKeyBinding("k", PrintKDown)
	gil.AddButtonBinding(1, PrintAButtonDown)
	gil.AddTimerBeganCallback(PrintTimerBegin)
	gil.AddTimerEndedCallback(PrintTimerEnded)
	gil.AddQuitKey(pygame.K_j)

	#game loop
	while (gil.Listen()):
		clock.tick(120)

	#do both to quit properly
	pygame.quit()
	sys.exit()
		