import sys, keyboard, pygame, subprocess, yaml, psutil, time
from threading import Timer

with open("config.yml", 'r') as stream:
	try:
		config = yaml.load(stream)
	except yaml.YAMLError as exc:
		print(exc)

def WindowExists( exe_name ):
	for pid in psutil.pids(): 
		p = psutil.Process(pid)
		name = p.name()
		if name == exe_name:
			return True

class GameInputListener:

	def __init__(self, time_limit=-1):
		self.keys = []
		self.key_states = dict()
		self.key_actions = dict()

		self.buttons = []
		self.button_states = dict()
		self.button_actions = dict()

		self.quit_flag = False
		self.timer_active = False
		self.timer_began_callback = lambda:None
		self.timer_ended_callback = lambda:None
		if (not (type(time_limit) is int)) or time_limit <= 0:
			self.time_limit = -1
		else:
			self.time_limit = time_limit
		self.start_time = 0

	def __del__(self):
		self.Dispose()

	def Dispose(self):
		self.timer_began_callback = None
		self.timer_ended_callback = None
		self.keys = []
		self.buttons = []
		self.key_states.clear()
		self.button_states.clear()
		self.key_actions.clear()
		self.button_actions.clear()
		try:
			keyboard.unhook_all()
		except:
			raise

	def Listen(self):
		key_pressed = False
		current_ticks = pygame.time.get_ticks()
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				gil.Dispose()
				return False
			elif event.type == pygame.KEYDOWN:
				key_pressed = True
				if event.key in self.keys:
					if self.key_states[event.key]:
						self.key_actions[event.key]()
			elif event.type == pygame.JOYBUTTONDOWN:
				key_pressed = True
				if event.button in self.buttons:
					if self.button_states[event.button]:
						self.button_actions[event.button]()
		if self.quit_flag:
			gil.Dispose()
			return False
		else:
			if self.timer_active:
				if key_pressed:
					self.start_time = current_ticks
				if self.time_limit > 0:
					if current_ticks - self.start_time >= self.time_limit:
						self.StopTimer()
			else:
				if key_pressed:
					self.StartTimer(current_ticks)
		return True

	def AddTimerBeganCallback(self, callback):
		if callable(callback):
			self.timer_began_callback = callback;

	def AddTimerEndedCallback(self, callback):
		if callable(callback):
			self.timer_ended_callback = callback;

	def StartTimer(self, ticks):
		if self.time_limit > 0:
			self.start_time = ticks
			self.timer_active = True
			self.timer_began_callback()

	def StopTimer(self):
		if self.time_limit > 0:
			self.timer_active = False
			self.timer_ended_callback()

	def Quit(self):
		self.quit_flag = True

	def AddQuitKey(self, key):
		self.AddKeyBinding(key, self.Quit)

	def AddQuitButton(self, button):
		self.AddButtonBinding(button, self.Quit)

	def Key_Callback(self, key):
		if key in self.keys:
			if self.key_states[key]:
				key_event = pygame.event.Event(pygame.KEYDOWN, {'key':key})
				pygame.event.post(key_event)

	def AddKeyBinding(self, key, callback):
		if not callable(callback):
			self.Dispose()
			raise TypeError("callback argument in AddKeyBinding is not a callable function")
		try:
			action_callback = lambda:self.Key_Callback(key)
			keyboard.hook_key(key, action_callback)
			if not (key in self.keys):
				self.keys.append(key)
				self.key_states[key] = True
			self.key_actions[key] = callback
		except:
			self.Dispose()
			raise

	def PauseKeyBinding(self, key):
		if key in self.keys:
			self.key_states[key] = False

	def ResumeKeyBinding(self, key):
		if key in self.keys:
			self.key_states[key] = True

	def AddButtonBinding(self, button, callback):
		if not callable(callback):
			self.Dispose()
			raise TypeError("callback argument in AddButtonBinding is not a callable function")
		if not (button in self.buttons): 
			self.buttons.append(button)
			self.button_states[button] = True
		self.button_actions[button] = callback

# static variables
game_show = config["quiz"]["exe"]
game_show_args = "-nosnapshot -settingsfile="
led_blinky = config["led"]["exe"]
quiz = config["quiz"]["file"]
global game_mode
game_mode = ""

def RunQXP( game_config, led_options):
	args = "\"" + game_show + "\" " + game_show_args + game_config + " \"" + quiz + "\""
	subprocess.Popen( args )
	StartLEDBlinky( led_options )

def StopQXP():
	if WindowExists( "Quiz Show.exe" ):
		subprocess.Popen( "taskkill /F /IM \"Quiz Show.exe\"" )
		StopLEDBlinky()
		return True

def StartLEDBlinky( options ):
	args = led_blinky + " 1"
	subprocess.Popen( args )

	time.sleep(3)

	args = led_blinky + " 14 " + options
	subprocess.Popen( args )

def StopLEDBlinky():
	subprocess.Popen( led_blinky + " 2" )		

# Callbacks
def Guided():
	global game_mode

	gil.PauseKeyBinding("g")

	if StopQXP():
		time.sleep(4)

	game_mode = "guided"

	RunQXP( config["quiz"][game_mode]["config"], config["led"][game_mode]["options"] )

	gil.ResumeKeyBinding("g")

def Standalone():
	global game_mode

	gil.PauseKeyBinding("s")

	if StopQXP():
		time.sleep(4)

	game_mode = "standalone"

	RunQXP( config["quiz"][game_mode]["config"], config["led"][game_mode]["options"] )

	gil.ResumeKeyBinding("s")

def Reset():

	if StopQXP():
		time.sleep(4)
		gil.PauseKeyBinding("r")

		global game_mode
				
		if game_mode == "guided":
			Guided()
		elif game_mode == "standalone":
			Standalone()

		gil.ResumeKeyBinding("r")

def TimerBegin():
	args = led_blinky + " 6"
	subprocess.Popen( args )

def TimerEnded():
	args = led_blinky + " 5"
	subprocess.Popen( args )

if __name__ == "__main__":
	pygame.init()
	pygame.event.set_grab(True)
	#pygame.display.set_mode((1, 1))
	
	pygame.joystick.init()
	controller = pygame.joystick.Joystick(0)
	controller.init()
	clock = pygame.time.Clock()

	#setup the listener
	gil = GameInputListener(12000)
	gil.StartTimer( pygame.time.get_ticks() )
	gil.AddKeyBinding("g", Guided);
	gil.AddKeyBinding("s", Standalone);
	gil.AddKeyBinding("r", Reset);

	gil.AddTimerBeganCallback(TimerBegin)
	gil.AddTimerEndedCallback(TimerEnded)

	gil.AddQuitKey(pygame.K_q)

	#game loop
	while (gil.Listen()):
		clock.tick(120)

	#do both to quit properly
	pygame.quit()
	sys.exit()
